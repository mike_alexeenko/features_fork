<?php
/**
 * @file
 * about_the_author.features.inc
 */

/**
 * Implements hook_views_api().
 */
function about_the_author_views_api($module = NULL, $api = NULL) {
  return array("api" => "3.0");
}
